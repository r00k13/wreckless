﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControl : MonoBehaviour
{
    public Transform leftBrake;
    public Transform rightBrake;

    public GameObject thrustersMain;
    public GameObject thrustersLeft;
    public GameObject thrustersRight;

    public Node nextNode;
    protected Node lastNode;
    private float timeFlipped = 0f;

    private void Start()
    {
        lastNode = nextNode;
    }

    private void Update()
    {
        if((transform.eulerAngles.z >= 120f && transform.eulerAngles.z <= 240f)|| (transform.eulerAngles.z <= -120f && transform.eulerAngles.z >= -240f))
        {
            timeFlipped += Time.deltaTime;

            if (timeFlipped >= 5f)
            {
                //transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0);
                transform.position = lastNode.transform.position;
                transform.rotation = lastNode.transform.rotation;
                //Debug.Log(gameObject.name);
            }
        }
        else
        {
            timeFlipped = 0f;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Wall")
        {
            GetComponent<Rigidbody>().AddForce((transform.position - collision.contacts[0].point) * 10, ForceMode.Impulse);
        }
    }

    public virtual void Thrust(int force)
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * force * Time.deltaTime);
        thrustersMain.SetActive(true);
    }

    public virtual void AirbrakeLeft(float force)
    {
        GetComponent<Rigidbody>().AddTorque(transform.up * -2 * force * Time.deltaTime);
        GetComponent<Rigidbody>().AddTorque(transform.forward * (force / 5) * Time.deltaTime);
        GetComponent<Rigidbody>().AddForceAtPosition((transform.position - leftBrake.position) * force * Time.deltaTime, leftBrake.position);
        thrustersRight.SetActive(true);
    }

    public virtual void AirbrakeRight(float force)
    {
        GetComponent<Rigidbody>().AddTorque(transform.up * 2 * force * Time.deltaTime);
        GetComponent<Rigidbody>().AddTorque(transform.forward * -1 * (force /5) * Time.deltaTime);
        GetComponent<Rigidbody>().AddForceAtPosition((transform.position - rightBrake.position) * force * Time.deltaTime, rightBrake.position);
        thrustersLeft.SetActive(true);
    }
}
