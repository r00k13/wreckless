﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : ShipControl
{
    void FixedUpdate()
    {
        if (Input.GetButton("Thrust"))
        {
            Thrust(16000);
        }
        else
        {
            thrustersMain.SetActive(false);
        }

        if (Input.GetAxis("AirbrakeLeft") > 0)
        {
            AirbrakeLeft(1000 * Input.GetAxis("AirbrakeLeft"));
        }
        else
        {
            thrustersRight.SetActive(false);
        }

        if (Input.GetAxis("AirbrakeRight") > 0)
        {
            AirbrakeRight(1000 * Input.GetAxis("AirbrakeRight"));
        }
        else
        {
            thrustersLeft.SetActive(false);
        }

        if (Vector3.Distance(transform.position, nextNode.transform.position) < 100f)
        {
            if (nextNode.nextNode != null)
            {
                lastNode = nextNode;
                nextNode = nextNode.nextNode;
            }
        }
        //Vector3 oldRot = transform.rotation.eulerAngles;
        //transform.rotation = Quaternion.Euler(oldRot.x, oldRot.y, 0);
    }
}
