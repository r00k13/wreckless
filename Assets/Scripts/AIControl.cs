﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



//http://answers.unity3d.com/questions/254130/how-do-i-rotate-an-object-towards-a-vector3-point.html
//https://docs.unity3d.com/ScriptReference/Quaternion.Angle.html
//https://forum.unity3d.com/threads/left-right-test-function.31420/

public class AIControl : ShipControl
{
    
    private Vector3 target;

    private void Start()
    {
        target = nextNode.transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (Vector3.Distance(transform.position, target) > 200f)
        {
            Thrust(16000);
        }
        else if (Vector3.Distance(transform.position, target) > 100f)
        {
            Thrust(14000);
        }
        else
        {
            thrustersMain.SetActive(false);
            if(nextNode.nextNode != null)
            {
                lastNode = nextNode;
                nextNode = nextNode.nextNode;
                target = nextNode.transform.position + new Vector3(Random.Range(-20f, 20f), 0, Random.Range(-20f, 20f));
            }
        }

        Vector3 direction = (nextNode.transform.position - transform.position).normalized;  //find the vector pointing from our position to the target
        Quaternion lookRotation = Quaternion.LookRotation(direction);                       //create the rotation we need to be in to look at the target
        float angle = Quaternion.Angle(transform.rotation, lookRotation);
        float dirNum = AngleDir(transform.forward, direction, transform.up);

        if ((angle * dirNum) < -5f)
        {
            AirbrakeLeft(1000 * (angle / 10));
            thrustersLeft.SetActive(false);
        }
        else if ((angle * dirNum) > 5f)
        {
            AirbrakeRight(1000 * (angle / 10));
            thrustersRight.SetActive(false);
        }
        else
        {
            thrustersLeft.SetActive(false);
            thrustersRight.SetActive(false);
        }
    }

    float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0f)
        {
            return 1f;
        }
        else if (dir < 0f)
        {
            return -1f;
        }
        else
        {
            return 0f;
        }
    }
}
