﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Transform target;

    public float distance, heightDist, rotationDamping, distanceDamping;

    void LateUpdate()
    {
        Vector3 currentPosition = transform.position;
        Vector3 targetPosition = target.transform.position;

        float currentHeight = Mathf.Lerp(currentPosition.y, targetPosition.y + heightDist, distanceDamping * Time.deltaTime);

        float currentRotation = transform.eulerAngles.y;
        float targetRotation = target.transform.eulerAngles.y;

        currentRotation = Mathf.LerpAngle(currentRotation, targetRotation, rotationDamping * Time.deltaTime);

        Quaternion followingRotation = Quaternion.Euler(0.0f, currentRotation, 0.0f);

        transform.position = targetPosition;

        transform.position -= followingRotation * Vector3.forward * distance;

        transform.position = new Vector3(transform.position.x, currentHeight, transform.position.z);

        transform.LookAt(target.transform);
    }
}
