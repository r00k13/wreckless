﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public ShipControl[] ships;
    public Text countdown;

	void Update ()
    {
		if(Time.timeSinceLevelLoad > 3f)
        {
            for(int i = 0; i < ships.Length; i++)
            {
                ships[i].enabled = true;
                countdown.gameObject.SetActive(false);
            }
        }
        else
        {
            countdown.text = "" + (3 - Mathf.FloorToInt(Time.timeSinceLevelLoad));
        }
	}
}
